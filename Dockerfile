# vim:set ft=dockerfile:
FROM php:7.3.27-apache-buster

RUN apt update && apt install -y \
    git \
    nano \
    unzip

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync

# install the PHP extensions we need
RUN set -eux; \
    \
    if command -v a2enmod; then \
    a2enmod rewrite; \
    fi; \
    \
    savedAptMark="$(apt-mark showmanual)"; \
    \

    install-php-extensions \
    bcmath \
    bz2 \
    calendar \
    dba \
    exif \
    gd \
    gettext \
    gmp \
    iconv \
    igbinary \
    imagick \
    imap \
    intl \
    mbstring \
    memcached \
    msgpack \
    mysqli \
    opcache \
    pcntl \
    pdo_mysql \
    pgsql \
    shmop \
    soap \
    sockets \
    sysvmsg \
    sysvsem \
    sysvshm \
    wddx \
    xsl \
    zip \
    ; \
    \

    # reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual $savedAptMark; \
    ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
    | awk '/=>/ { print $3 }' \
    | sort -u \
    | xargs -r dpkg-query -S \
    | cut -d: -f1 \
    | sort -u \
    | xargs -rt apt-mark manual; \
    \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
    rm -rf /var/lib/apt/lists/*

COPY ./php-mcp.ini /usr/local/etc/php/conf.d

# https://github.com/drupal/drupal/blob/9.0.1/composer.lock#L4052-L4053
COPY --from=composer:1.10 /usr/bin/composer /usr/local/bin/

RUN usermod -u 1000 www-data

# Installing Drush and Drupal Console
RUN curl https://github.com/drush-ops/drush/releases/download/8.4.5/drush.phar -L -o drush.phar && \
    chmod +x ./drush.phar && \
    mv drush.phar /usr/local/bin/drush


